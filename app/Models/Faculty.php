<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Ramsey\Collection\Collection;

/**
 * @property int id
 * @property string name
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 *
 * @property Student|Collection students
 *
 * Class Faculty
 * @package App\Models
 */
final class Faculty extends Model
{
    use HasFactory;

    public function students(): HasMany
    {
        return $this->hasMany(Student::class);
    }
}
