<?php

namespace App\Models;

use App\Filters\QueryFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int id
 * @property string name
 * @property int teacher_id
 * @property int group_id
 * @property int faculty_id
 * @property Carbon|string $birthday
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 *
 * @property float|null avg_grade
 * @property Teacher|null teacher
 * @property Faculty|null faculty
 * @property Group|null group
 * @property Grade|Collection grades
 *
 * Class Student
 * @package App\Models
 */
final class Student extends Model
{
    use HasFactory;

    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    public function faculty(): BelongsTo
    {
        return $this->belongsTo(Faculty::class);
    }

    public function group(): BelongsTo
    {
        return $this->belongsTo(Group::class);
    }

    public function grades(): HasMany
    {
        return $this->hasMany(Grade::class);
    }

    public function getAvgGradeAttribute(): float|null
    {
        return $this->grades->average('grade');
    }

    public function scopeFilter(Builder $builder, QueryFilter $filters): Builder
    {
        return $filters->apply($builder);
    }
}
