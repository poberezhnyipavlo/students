<?php

namespace App\Services;

use App\Filters\QueryFilter;

abstract class BaseService
{
    public const OFFSET_DEFAULT = 0;
    public const LIMIT_DEFAULT = 10;

    public QueryFilter $queryFilter;
}
