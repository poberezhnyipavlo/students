<?php

namespace App\Services;

use App\Filters\StudentFilter;
use App\Models\Student;
use Illuminate\Support\Collection;

final class StudentService extends BaseService
{
    public function __construct(StudentFilter $studentFilter)
    {
        $this->queryFilter = $studentFilter;
    }

    public function getAll(array $params): Collection
    {
        return Student::filter($this->queryFilter)
            ->with([
                'teacher',
                'group',
                'faculty',
            ])
            ->skip($params['offset'] ?? self::OFFSET_DEFAULT)
            ->take($params['limit'] ?? self::LIMIT_DEFAULT)
            ->get();
    }

    public function create(array $data): Student
    {
        $student = new Student();

        $student->name = $data['name'];
        $student->birthday = $data['birthday'];
        $student->teacher_id = $data['teacher_id'];
        $student->faculty_id = $data['faculty_id'];
        $student->group_id = $data['class_id'];

        if ($student->save()) {
            return $student;
        }
    }
}
