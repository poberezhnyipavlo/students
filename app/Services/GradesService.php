<?php

namespace App\Services;

use App\Filters\GraderFilter;
use App\Models\Grade;
use App\Models\Student;
use Illuminate\Support\Collection;

final class GradesService extends BaseService
{
    public function __construct(GraderFilter $graderFilter)
    {
        $this->queryFilter = $graderFilter;
    }

    public function getForStudent(Student $student, array $params): Collection
    {
        return Grade::filter($this->queryFilter)
            ->where('student_id', $student->id)
            ->with(['teacher', 'subject'])
            ->skip($params['offset'] ?? self::OFFSET_DEFAULT)
            ->take($params['limit'] ?? self::LIMIT_DEFAULT)
            ->get();
    }

    public function createGroStudent(array $data, Student $student): Grade
    {
        $grade = new Grade();

        $grade->subject_id = $data['subject_id'];
        $grade->student_id = $student->id;
        $grade->grade = $data['grade'];
        $grade->teacher_id = $data['teacher_id'];
        $grade->date = $data['date'];

        $grade->save();

        return $grade;
    }
}
