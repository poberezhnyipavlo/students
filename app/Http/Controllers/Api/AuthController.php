<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AuthRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(AuthRequest $request): JsonResponse
    {
        Auth::attempt($request->validated());

        /** @var User $user */
        $user = auth()->user();

        if ($user) {
            return response()->json([
                'token' => $user->createToken('bearer')->plainTextToken,
            ], 200);
        }

        return response()->json(['error' => 'Ви ввели не правильний емейл або пароль'], 401);
    }
}
