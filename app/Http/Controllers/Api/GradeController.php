<?php

namespace App\Http\Controllers\Api;

use App\Filters\GraderFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\GradeGetRequest;
use App\Http\Requests\GradeRequest;
use App\Http\Resources\Grade\GradeCollection;
use App\Models\Student;
use App\Services\GradesService;
use Illuminate\Http\JsonResponse;

final class GradeController extends Controller
{
    private GradesService $gradesService;

    public function __construct(GradesService $gradesService)
    {
        $this->gradesService = $gradesService;
    }

    public function getForGraderForStudent(GradeGetRequest $request, Student $student): JsonResponse
    {
        return response()->json(
            new GradeCollection(
                $this->gradesService->getForStudent($student, $request->all()),
            ),
        );
    }

    public function storeGradeForStudent(GradeRequest $request, Student $student): JsonResponse
    {
        $this->gradesService->createGroStudent($request->validated(), $student);

        return response()->json();
    }
}
