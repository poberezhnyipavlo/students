<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentGetRequest;
use App\Http\Requests\StudentRequest;
use App\Http\Resources\Student\StudentCollection;
use App\Services\StudentService;
use Illuminate\Http\JsonResponse;

final class StudentController extends Controller
{
    private StudentService $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
    }

    public function index(StudentGetRequest $request): JsonResponse
    {
        return response()->json(
            new StudentCollection(
                $this->studentService->getAll($request->all()),
            ),
        );
    }

    public function store(StudentRequest $request): JsonResponse
    {
        $this->studentService->create($request->validated());

        return response()->json();
    }
}
