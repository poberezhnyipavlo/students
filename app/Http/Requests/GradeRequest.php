<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class GradeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'subject_id' => [
                'required',
                'int',
                Rule::exists('subjects', 'id'),
            ],
            'grade' => [
                'required',
                'numeric',
                'max:5',
            ],
            'teacher_id' => [
                'required',
                'int',
                Rule::exists('teachers', 'id'),
            ],
            'date' => [
                'required',
                'date',
            ],
        ];
    }
}
