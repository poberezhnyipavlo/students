<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class StudentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
                'max:60',
            ],
            'class_id' => [
                'required',
                'int',
                Rule::exists('groups', 'id'),
            ],
            'birthday' => [
                'required',
                'date',
            ],
            'faculty_id' => [
                'required',
                'int',
                Rule::exists('faculties', 'id'),
            ],
            'teacher_id' => [
                'required',
                'int',
                Rule::exists('teachers', 'id'),
            ],
        ];
    }
}
