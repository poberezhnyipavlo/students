<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

final class StudentGetRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'faculty' => [
                'string',
                Rule::exists('faculties', 'name'),
            ],
            'teacher' => [
                'string',
                Rule::exists('teachers', 'name'),
            ],
            'age' => [
                'int',
            ],
            'limit' => [
                'int',
            ],
            'offset' => [
                'int',
            ],
        ];
    }
}
