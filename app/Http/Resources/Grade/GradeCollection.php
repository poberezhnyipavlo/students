<?php

namespace App\Http\Resources\Grade;

use Illuminate\Http\Resources\Json\ResourceCollection;

final class GradeCollection extends ResourceCollection
{
    public function toArray($request): ResourceCollection
    {
        return GradeResource::collection($this->collection);
    }
}
