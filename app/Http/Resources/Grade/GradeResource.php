<?php

namespace App\Http\Resources\Grade;

use App\Models\Grade;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class GradeResource
 * @mixin Grade
 * @package App\Http\Resources\Grade
 */
final class GradeResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'subject' => $this->subject->name,
            'grade' => $this->grade,
            'teacher' => $this->teacher->name,
            'date' => $this->date,
        ];
    }
}
