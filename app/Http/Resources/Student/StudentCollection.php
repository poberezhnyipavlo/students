<?php

namespace App\Http\Resources\Student;

use Illuminate\Http\Resources\Json\ResourceCollection;

final class StudentCollection extends ResourceCollection
{
    public function toArray($request): ResourceCollection
    {
        return StudentResource::collection($this->collection);
    }
}
