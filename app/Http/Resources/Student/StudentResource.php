<?php

namespace App\Http\Resources\Student;

use App\Models\Student;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class StudentResource
 * @mixin Student
 * @package App\Http\Resources
 */
final class StudentResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'class' => $this->group->name,
            'birthday' => $this->birthday,
            'faculty' => $this->faculty->name,
            'teacher' => $this->teacher->name,
            'avg_grade' => $this->avg_grade,
        ];
    }
}
