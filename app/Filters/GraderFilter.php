<?php

namespace App\Filters;

use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Database\Eloquent\Builder;

final class GraderFilter extends QueryFilter
{
    public function subject(string $name): Builder
    {
        /** @var Subject $subject */
        $subject = Subject::query()->where('name', $name)->first();

        return $this->builder->where('subject_id', $subject->id);
    }

    public function teacher(string $name): Builder
    {
        /** @var Teacher $teacher */
        $teacher = Teacher::query()->where('name', $name)->first();

        $this->builder->where('subject_id', $teacher->id);
    }
}
