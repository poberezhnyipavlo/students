<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class QueryFilter
{
    protected Request $request;

    public Builder $builder;

    private string $delimiter = ',';

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function filters(): array|string|null
    {
        return $this->request->query();
    }

    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->filters() as $name => $value) {
            if (method_exists($this, $name)) {
                call_user_func_array([$this, $name], array_filter([$value]));
            }
        }

        $this->sort();

        return $this->builder;
    }

    protected function paramToArray(string $param): array
    {
        return explode($this->delimiter, $param);
    }

    protected function sort(): Builder
    {
        $order = $this->filters()['sort_order'] ?? 'id';
        $sort = $this->filters()['sort_by'] ?? 'asc';

        return $this->builder->orderBy($sort, $order);
    }
}
