<?php

namespace App\Filters;

use App\Models\Faculty;
use App\Models\Teacher;
use Illuminate\Database\Eloquent\Builder;

final class StudentFilter extends QueryFilter
{
    public function faculty(string $name): Builder
    {
        /** @var Faculty $faculty */
        $faculty = Faculty::query()->where('name', $name)->first();

        return $this->builder->where('faculty_id', $faculty->id);
    }

    public function teacher(string $name): Builder
    {
        /** @var Teacher $teacher */
        $teacher = Teacher::query()->where('name', $name)->first();

        return $this->builder->where('teacher_id', $teacher->id);
    }

    public function age(int $age): Builder
    {
        return $this->builder->whereRaw('floor(DATEDIFF(CURDATE(), birthday) / 365) = ' . $age);
    }
}
