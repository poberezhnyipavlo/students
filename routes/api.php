<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\GradeController;
use App\Http\Controllers\Api\StudentController;
use Illuminate\Support\Facades\Route;

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(static function () {
   Route::get('students', [StudentController::class, 'index']);
   Route::post('students/add', [StudentController::class, 'store']);

   Route::get('grades/{student}/get', [GradeController::class, 'getForGraderForStudent']);
   Route::post('grades/{student}/add', [GradeController::class, 'storeGradeForStudent']);
});
