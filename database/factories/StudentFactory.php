<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

final class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws \Exception
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'birthday' => $this->faker->dateTime,
            'faculty_id' => random_int(1, 5),
            'group_id' => random_int(1, 5),
            'teacher_id' => random_int(1, 5),
        ];
    }
}
