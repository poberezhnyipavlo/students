<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;

final class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Group::factory(5)->create();
    }
}
