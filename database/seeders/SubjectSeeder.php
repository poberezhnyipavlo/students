<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

final class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Subject::create([
            'name' => 'mathematics',
            'teacher_id' => 1,
        ]);

        Subject::create([
            'name' => 'philosophy',
            'teacher_id' => 2,
        ]);

        Subject::create([
            'name' => 'physical culture',
            'teacher_id' => 3,
        ]);

        Subject::create([
            'name' => 'english',
            'teacher_id' => 4,
        ]);

        Subject::create([
            'name' => 'informatics',
            'teacher_id' => 5,
        ]);
    }
}
